import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { TokenStorageService } from '../_services/token-storage.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

    constructor(
        private router: Router,
        private tokenStorageService: TokenStorageService,
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        console.log(route)
        let has_access: boolean = false;
        let isLoggedIn = !!this.tokenStorageService.getToken();
        if (isLoggedIn) {
          const user = this.tokenStorageService.getUser();
          let roles = user.roles;
    
          has_access = roles.includes('ROLE_ADMIN');
        }
        

        return has_access;
    }
}
