import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminInstitutionComponent } from './_components/admin-institution/admin-institution.component';
import { HomeComponent } from './_components/home/home.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'institution/:id', component: AdminInstitutionComponent},
  { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
