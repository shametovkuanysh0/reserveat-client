import { Component, Input, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DbService } from 'src/app/_services/db.service';
import { FileService } from 'src/app/_services/file.service';

@Component({
  selector: 'admin-table',
  templateUrl: './admin-table.component.html',
  styleUrls: ['./admin-table.component.css']
})
export class AdminTableComponent implements OnInit {
  @Input('show_add') show_add: boolean = false
  @Input('show_delete') show_delete: boolean = false
  @Input('table_name') table_name: string = ""
  @Input() defaultData: any = {}
  loading: boolean = false
  data: any[] = []
  columns: any[] = []
  totalRecords = 0
  showDetail:boolean = false
  form: any = {};
  

  constructor(private fileService: FileService,
              private dbService: DbService,
              private messageService: MessageService) { }

  ngOnInit(): void {
    this.loadData(null)
  }

  loadData(event:any){
    this.loading = true
    this.dbService.getDataAll(this.table_name).subscribe(data => {
      if (data){
        this.columns = data.cols
        this.totalRecords = data.data.length
        this.data = data.data
        this.loading = false
      }
    }) 
  }

  edit(data:any){
    this.form = data
    this.showDetail = true
    this.fileService.getFilesOfTable(this.table_name,data._id).subscribe(res => {
      if (res){
        data['_files'] = res
      }
    })
  }

  delete(data:any){
    this.dbService.deleteData(this.table_name, data['_id']).subscribe(res=>{
      this.loadData(null)
    })
  }

  hideDialog(){
    this.showDetail = false
  }

  saveData(){
    let valid = this.checkValidation()
    if (valid){
      if (this.form['_id']){
        this.dbService.updateData(this.table_name, this.form['_id'], this.form).subscribe(data => {
          this.form = {}
          this.showDetail = false
          this.loadData(null)
        },
        err => {
          this.messageService.add({severity:'error', summary: 'Ошибка', detail:  err.error.message});
        })
      } else {
        this.dbService.insertData(this.table_name, this.form).subscribe(data => {
          this.form = {}
          this.showDetail = false
          this.loadData(null)
        },
        err => {
          this.messageService.add({severity:'error', summary: 'Ошибка', detail:  err.error.message});
        })
      }
    } else {
      this.messageService.add({severity:'error', summary: 'Ошибка', detail:  'Проверьте форму'});
    }
  }

  checkValidation(){
    let valid = true
    this.columns.forEach(col => {
      col.errors = []
      if (col.required && !this.form[col.name]){
        col.errors.push({hasError: true, type: "required"})
        valid = false
      }
    })
    return valid
  }

  search(e:any, column:any){
    this.dbService.getDataAll(column.table_name).subscribe(res => {
      column.data = res.data
    })
  }

  addNew(){
    this.showDetail = true
    this.form = this.defaultData
  }

  onUpload(e:any){
    e.files.forEach((file:any) => {
      this.fileService.uploadFile(this.table_name, this.form['_id'], file).subscribe(data => {
        this.form = {}
        this.showDetail = false
        this.loadData(null)
      },
      err => {
        this.messageService.add({severity:'error', summary: 'Ошибка', detail:  err.error.message});
      })
    });
  }
}
