import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import { CompanyService } from 'src/app/_services/company.service';
import { DbService } from 'src/app/_services/db.service';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'admin-institution',
  templateUrl: './admin-institution.component.html',
  styleUrls: ['./admin-institution.component.css']
})
export class AdminInstitutionComponent implements OnInit {
  id: any = 0
  institution: any = {}
  columns: any[] = []
  form: any = {};
  table_name="institution"

  constructor(private userService: UserService,
              private dbService: DbService,
              private messageService: MessageService,
              private router: ActivatedRoute) { }

  ngOnInit(): void {
    this.router.params.subscribe(data => {
      this.id = data['id']
      this.loadInstitution(this.id)
    })
  }

  loadInstitution(id:any){
    if (this.id == 0){
      this.dbService.getAttrs(this.table_name).subscribe(data => {
        this.columns = data
      })
    } else {
      this.dbService.getData(this.table_name,this.id).subscribe(data => {
        this.institution = data.data
        this.columns = data.cols
        this.form = this.institution
      })
    }
  }

  saveData(){
    let valid = this.checkValidation()
  
    if (valid){
      if (this.form['_id']){
        this.dbService.updateData('institution', this.form['_id'], this.form).subscribe(data => {
          this.form = {}
          this.loadInstitution(this.id)
        },
        err => {
          this.messageService.add({severity:'error', summary: 'Ошибка', detail:  err.error.message});
        })
      } else {
        this.dbService.insertData('institution', this.form).subscribe(data => {
          this.form = {}
          window.location.href = `/admin/institution/${data.result._id}`
        },
        err => {
          this.messageService.add({severity:'error', summary: 'Ошибка', detail:  err.error.message});
        })
      }
    } else {
      this.messageService.add({severity:'error', summary: 'Ошибка', detail:  'Проверьте форму'});
    }
  }

  checkValidation(){
    let valid = true
    this.columns.forEach(col => {
      col.errors = []
      if (col.required && !this.form[col.name]){
        col.errors.push({hasError: true, type: "required"})
        valid = false
      }
    })
    return valid
  }

  search(e:any, col:any){
    this.dbService.getDataAll(col.table_name).subscribe(res => {
      col.data = res.data
    })
  }

  onUpload(e:any){
    console.log(e);
  }
}
