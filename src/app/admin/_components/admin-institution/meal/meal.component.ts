import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import { CompanyService } from 'src/app/_services/company.service';
import { DbService } from 'src/app/_services/db.service';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'meal',
  templateUrl: './meal.component.html',
  styleUrls: ['./meal.component.css']
})
export class MealComponent implements OnInit {
  @Input() institution:any = ''
  table_name = 'meal'
  form={}
  constructor(private userService: UserService,
              private dbService: DbService,
              private messageService: MessageService,
              private router: ActivatedRoute) { }

  ngOnInit(): void {
    this.form = {institution: this.institution['_id']}
  }
}
