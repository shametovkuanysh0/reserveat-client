import { Component, OnInit } from '@angular/core';
import { DbService } from 'src/app/_services/db.service';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'admin-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  institutions: Array<any> = []
  constructor(private userService: UserService,
              private dbService: DbService) { }

  ngOnInit(): void {
    this.loadIntitutions()
  }

  loadIntitutions(){
    this.dbService.getDataAll('institution').subscribe(data => {
      this.institutions = data.data
    })
  }

  addInstitution(){
    window.location.href = '/admin/institution/0'
  }

  openDetail(data:any){
    window.location.href = `/admin/institution/${data._id}`
  }

  blockInstitution(){
    
  }
}
