import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { HomeComponent } from './_components/home/home.component';
import { AdminTableComponent } from './_components/admin-table/admin-table.component';
import { AdminInstitutionComponent } from './_components/admin-institution/admin-institution.component';
import {FileUploadModule} from 'primeng/fileupload';
import {HttpClientModule} from '@angular/common/http';
// Prime
import {TabViewModule} from 'primeng/tabview';
import {TableModule} from 'primeng/table';
import {DialogModule} from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import {DropdownModule} from 'primeng/dropdown';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {CardModule} from 'primeng/card';
import {FieldsetModule} from 'primeng/fieldset';
import { MealComponent } from './_components/admin-institution/meal/meal.component';
import {GalleriaModule} from 'primeng/galleria';



@NgModule({
  declarations: [
    AdminComponent,
    HomeComponent,
    AdminTableComponent,
    AdminInstitutionComponent,
    MealComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ReactiveFormsModule,
    TabViewModule,
    FormsModule,
    TableModule,
    ButtonModule,
    DialogModule,
    InputTextModule,
    DropdownModule,
    AutoCompleteModule,
    CardModule,
    FileUploadModule,
    HttpClientModule,
    FieldsetModule,
    GalleriaModule
  ]
})
export class AdminModule { }
