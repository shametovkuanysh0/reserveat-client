import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = 'http://localhost:5000/api/company/';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  constructor(private http: HttpClient) { }

  getCompanies(): Observable<any> {
    return this.http.get(API_URL + 'all');
  }

  getCompany(id:any): Observable<any> {
    return this.http.get(API_URL + id);
  }

  createCompany(data:any): Observable<any> {
    return this.http.post(API_URL + '', data);
  }

  deleteCompany(id:any): Observable<any> {
    return this.http.delete(API_URL + id);
  }

  updateCompany(id:any, data:any): Observable<any> {
    return this.http.put(API_URL + id, data)
  }
}
