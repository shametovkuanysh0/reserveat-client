import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = 'http://localhost:5000/api/db/';

@Injectable({
  providedIn: 'root'
})
export class DbService {
  constructor(private http: HttpClient) { }

  getDataAll(table_name:string): Observable<any> {
    return this.http.get(API_URL + table_name);
  }

  getData(table_name:string, id:any): Observable<any> {
    return this.http.get(API_URL + table_name + '/' + id);
  }

  getDataParams(table_name:string, params:any): Observable<any> {
    return this.http.get(API_URL + 'params/' + table_name, params);
  }

  getAttrs(table_name:string): Observable<any> {
    return this.http.get(API_URL + 'attrs/' + table_name);
  }

  insertData(table_name:string, data:any): Observable<any> {
    return this.http.post(API_URL + table_name, data);
  }

  deleteData(table_name:string, id:string): Observable<any> {
    return this.http.delete(API_URL + table_name + '/' + id);
  }

  updateData(table_name:string, id:any, data:any): Observable<any> {
    return this.http.patch(API_URL + table_name + '/' + id, data)
  }
}
