import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const AUTH_API = 'http://localhost:5000/api/auth/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) { }

  login(login: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + 'signin', {
      login,
      password
    }, httpOptions);
  }

  register(login: string, first_name: string, last_name: string, email: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + 'signup', {
      login,
      first_name,
      last_name,
      email,
      password
    }, httpOptions);
  }
}
